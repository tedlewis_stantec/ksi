#---- tdr_hydrology_analyses.R

#- Copyright (C) 2022 Stantec Consulting Ltd. All rights reserved. 
# Original script developed by Ted Lewis(ted.lewis@stantec.com), Oct 2022

#---- Description of script
# Project: 123221820 - Ksi Lisims LNG
# Script objective(s): conduct common wrangling and plotting for hydrology data
# Inputs: hydro data from aquarius (using the 'stantecAquariusIntegrator' package)
# Outputs: csv's and plots


library(stantecAquariusIntegrator)
library(here)
library(tidyverse)
library(ggtext)
library(imputeTS)
library(lubridate)
library(cowplot)
library(scales)

rm(list = ls()) #delete the global environment

source(paste0(here(), "/r/common_functions.R")) #load commonly used functions


#-----------------------------------------
# Grab and wrangle data
#-----------------------------------------

make_hydrology_tdr <- function(
  ts_name,
  ws_area_km2,
  hydro_timestep_raw,
  ts_maxgap,
  min_complete_frac,
  start_date, #for aquarius data
  end_date,#for aquarius data
  plot_start_date,
  plot_end_date,
  site_id,
  max_q
  )
  { #begin function
  
  #make directories
  dir.create(paste0(here(), "/data/TDR/hydro/", site_id), showWarnings = FALSE)
  dir.create(paste0(here(), "/figures/TDR/", site_id), showWarnings = FALSE)
  
  data_dir <- paste0(here(), "/data/TDR/hydro/", site_id, "/")
  fig_dir <- paste0(here(), "/figures/TDR/", site_id, "/")
  
  #retrieve data from aqaurius, required the 'stantecAquariusTsGrabber' package
  q_raw <- stantec_aquarius_timeseries("admin", "Q5c1iVYHsd5N", ts_name, start_date, end_date, 7) %>% 
    make_complete_time_series("timestamp_local_tz", "15 min")
  save_csv(q_raw, paste0(data_dir, site_id, "_PRESERVED_AQUARIUS_DATA"))
  
  q_wrangled_15min <- q_raw %>% 
    select(datetime = timestamp_local_tz,
           discharge_m3s = value) %>% 
    mutate(discharge_interp_m3s = na_interpolation(discharge_m3s, option = "linear", maxgap = ts_maxgap)) %>% 
    vol_runoff("discharge_interp_m3s", ws_area_km2, hydro_timestep_raw*60) %>% 
    cumulative_sum("vol_m3", "csum_vol_m3") %>% 
    cumulative_sum("runoff_mm", "csum_runoff_mm") %>% 
    mutate(
      discharge_m3s = round(discharge_m3s, 3),
      discharge_interp_m3s = round(discharge_interp_m3s, 3),
      vol_m3 = round(vol_m3, 3),
      runoff_mm = round(runoff_mm, 3),
      csum_vol_m3 = round(csum_vol_m3, 0),
      csum_runoff_mm = round(csum_runoff_mm, 1)
    ) %>% 
    flag_raw_events(max_q)
  
  save_csv(q_wrangled_15min, paste0(data_dir, site_id, "q_wrangled_15min"))
  
  q_wrangled_15min_long <- q_wrangled_15min %>%
    select(-c(discharge_m3s, max_flag, min_flag, gt_1pt5_measured, gt_2pt0_measured)) %>% 
    pivot_longer(names_to = "parameter", cols = -"datetime") 
  
  
  
  # get field data
  field_dates <- stantec_aquarius_field_data(
    "admin", 
    "Q5c1iVYHsd5N",
    site_id, #site identifier
    start_date, #start time (local time)
    end_date, #end time (local time)
    7
  )
  
  #save wrangled 15-min data.
  #saveRDS(q_wrangled_15min_long, file = paste0(here(), "/data/TDR/hydro/", site_id, "_q_wrangled_15min_long.rds"))
  #saveRDS(q_wrangled_15min, file = paste0(here(), "/data/TDR/hydro/", site_id, "_q_wrangled_15min.rds"))
  #saveRDS(field_dates, file = paste0(here(), "/data/TDR/hydro/", site_id, "_field_dates.rds"))
  
  #load rds data. Run from here if you don't want fresh data from Aquarius
  #q_wrangled_15min_long <- readRDS(file = paste0(here(), "/data/TDR/hydro/", site_id, "_q_wrangled_15min_long.rds"))
  #q_wrangled_15min <- readRDS(file = paste0(here(), "/data/TDR/hydro/", site_id, "_q_wrangled_15min.rds"))
  #field_dates <- readRDS(file = paste0(here(), "/data/TDR/hydro/", site_id, "_field_dates.rds"))
  
  #-----------------------------------------
  # Plot 15-min data
  #-----------------------------------------
  
  gglayers <- list(
    geom_vline(xintercept=field_dates$timestamp_local_tz, linetype="dotted"),
    theme_bw(),
    xlab(""),
    theme(
      axis.title.y = ggtext::element_markdown(),
      panel.grid.minor.y = element_blank()
    ),
    scale_x_datetime(date_breaks = "months", labels = date_format("%b"), limits = c(plot_start_date, plot_end_date))
  )
  
  
  discharge_15_min_plot <- raw_timeseries_line_plotting_function(q_wrangled_15min_long, "discharge_interp_m3s", "Discharge<br>(m<sup>3</sup>/s)", gglayers)+
    geom_hline(aes(yintercept = 1.5*max_q), colour="orange", linetype="dotted", alpha = 1)+
    geom_hline(aes(yintercept = 2*max_q), colour="red", linetype="dotted", alpha = 1)
  vol_15_min_plot <-raw_timeseries_line_plotting_function(q_wrangled_15min_long, "vol_m3", "Volume<br>(m<sup>3</sup>)", gglayers)+
    scale_y_continuous(labels = comma)
  csum_vol_15_min_plot <-raw_timeseries_line_plotting_function(q_wrangled_15min_long, "csum_vol_m3", "Cumulative<br>Volume (m<sup>3</sup>)", gglayers)+
    scale_y_continuous(labels = scientific)
  runoff_15_min_plot <-raw_timeseries_line_plotting_function(q_wrangled_15min_long, "runoff_mm", "Runoff (mm)", gglayers)
  csum_runoff_15_min_plot <-raw_timeseries_line_plotting_function(q_wrangled_15min_long, "csum_runoff_mm", "Cumulative<br>Runoff (mm)", gglayers)+
    scale_y_continuous(labels = comma)+
    xlab("Date (2022)")
  
  all_15_min_plots <- plot_grid(discharge_15_min_plot, 
                                vol_15_min_plot,
                                csum_vol_15_min_plot,
                                runoff_15_min_plot,
                                csum_runoff_15_min_plot,
                                ncol=1,
                                align = "v"
                                )
  all_15_min_plots 
  
  save_fig(all_15_min_plots, paste0(fig_dir, site_id, "_all_15_min_plots.tif"), 6, 7.5)
  
  #-----------------------------------------
  # make a daily average & sum time series
  #-----------------------------------------
  
  q_wrangled_daily <- q_wrangled_15min %>% 
    select(datetime, discharge_interp_m3s) %>% 
    group_by(year = year(datetime), month = month(datetime), day = day(datetime)) %>% 
    summarize(
      datetime = as.Date(datetime[1]),
      discharge_interp_m3s_avg = mean(discharge_interp_m3s, na.rm = TRUE),
      n_raw = n() #of measurements in a day
    ) %>%
    ungroup() %>%
    mutate(
      frac_complete = n_raw/(1440/hydro_timestep_raw) #1440 = number of minutes in a day
    ) %>% 
    filter(frac_complete > min_complete_frac) %>%
    vol_runoff("discharge_interp_m3s_avg", ws_area_km2, 60*60*24) %>% #60 minutes in an hour, 24 hours in a day
    cumulative_sum("vol_m3", "csum_vol_m3") %>%  
    cumulative_sum("runoff_mm", "csum_runoff_mm") %>% 
    running_avg_function(7, "discharge_interp_m3s_avg") %>%
    mutate(
      discharge_interp_m3s_avg = round(discharge_interp_m3s_avg, 3),
      vol_m3 = round(vol_m3, 3),
      runoff_mm = round(runoff_mm, 3),
      csum_vol_m3 = round(csum_vol_m3, 0),
      csum_runoff_mm = round(csum_runoff_mm, 1),
      mean_7_day_discharge_interp_m3s_avg = round(mean_7_day_discharge_interp_m3s_avg, 3)
    ) %>% 
    flag_daily_events(max_q) %>% 
    select(-c(year, month, day, n_raw, frac_complete)) %>% 
    make_complete_time_series("datetime", "1 day")
  
  save_csv(q_wrangled_daily %>% rename(date = datetime), paste0(data_dir, site_id, "_q_wrangled_daily"))

  

  
  #-----------------------------------------
  # make a monthly average & sum time series
  #-----------------------------------------
  
  q_wrangled_monthly <- q_wrangled_daily %>% 
    select(datetime, discharge_interp_m3s_avg) %>% 
    group_by(year = year(datetime), month = month(datetime)) %>% 
    summarize(
      datetime = as.Date(datetime[1]),
      discharge_interp_m3s_avg = mean(discharge_interp_m3s_avg, na.rm = TRUE),
      ndays = n()
    ) %>%
    ungroup() %>% 
    vol_runoff("discharge_interp_m3s_avg", ws_area_km2, 60*60*24) %>% #60 minutes in an hour, 24 hours in a day
    mutate(
      frac_complete = ndays/days_in_month(month),
      vol_m3 = vol_m3*ndays,
      runoff_mm = runoff_mm*ndays
    ) %>% 
    select(-c(year)) %>% 
    filter(frac_complete > min_complete_frac)
  
  save_csv(q_wrangled_monthly, paste0(data_dir, site_id, "_q_wrangled_monthly"))

  #boxplots of daily discharge
  
  q_wrangled_daily$fakedate = as.Date(paste(year(q_wrangled_daily$datetime), month(q_wrangled_daily$datetime), 1, sep="-"))
  
  daily_q_boxplots <- ggplot(data = q_wrangled_daily %>% filter(month(datetime) %in% q_wrangled_monthly$month))+ #filter out incomplete months
    geom_boxplot(aes(y=discharge_interp_m3s_avg), fill = "gray70")+
    facet_wrap(~fakedate, labeller = function(x) format(x, '%b'))+
    theme_bw()+
    theme(
      panel.grid.major = element_blank(),
      panel.grid.minor = element_blank(),
      axis.title.y = element_markdown(),
      axis.title.x=element_blank(),
      axis.text.x=element_blank(),
      axis.ticks.x=element_blank()
    )+
    ylab("Daily Average<br>Discharge (m<sup>3</sup>/s)")
  daily_q_boxplots
  
  save_fig(daily_q_boxplots, paste0(fig_dir, site_id, "_daily_q_boxplots.tif"), 4, 2.5)

  #flow duration curves
  fdc <- fdc_data_function(
    start_date, #start_date
    end_date, #end_date
    q_wrangled_daily, #time series name
    site_id #station
  )
  save_csv(fdc, paste0(data_dir, site_id, "_flow_duration"))
  
  #-----------------------------------------
  # get a rating table
  #-----------------------------------------
  rating_table <- stantec_aquarius_rating_table("admin","Q5c1iVYHsd5N", "WC-04", start_date, end_date, 7)
  save_csv(rating_table, paste0(data_dir, site_id, "_rating_table"))
  return(q_wrangled_daily)
  
} #end function


wc_04_tdr <- make_hydrology_tdr(
  "Discharge.Recorded@WC-04", #ts_name
  0.98, #ws_area_km2,
  15, #hydro_timestep_raw,
  12, #ts_maxgap,
  0.9, #min_complete_frac,
  as.POSIXct("2022-05-11"), #data start_date
  as.POSIXct("2022-11-16"), #date end_date
  as.POSIXct("2022-05-11"), #plot start_date
  as.POSIXct("2022-11-16"), #plot end_date
  "WC-04", #site_id
  0.126 #max measured Q
  )


wc_09_tdr <- make_hydrology_tdr(
  "Discharge.Recorded@WC-09", #ts_name
  9.2, #ws_area_km2,
  15, #hydro_timestep_raw,
  12, #ts_maxgap,
  0.9, #min_complete_frac,
  as.POSIXct("2022-03-01"), #data start_date
  as.POSIXct("2022-08-30 01:15"), #data end_date
  as.POSIXct("2022-03-01"), #plot start_date,
  as.POSIXct("2022-08-30 01:15"), #plot end_date,
  "WC-09", #site_id
  1.367#max measured Q
  )



